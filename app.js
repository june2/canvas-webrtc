var path = require('path');
var express = require('express');
var fs = require('fs');
// var mysql = require('mysql');

var app = require('express.io')();
// var connect = require('connect');

//feedback msg
var msg = 'success';

/**
 *  mysql connection config
 */
// var connection = mysql.createConnection({
// 	host : '121.160.208.118',
// 	port : 3306,
// 	user : 'mountain',
// 	password : 'mountain',
// 	database : 'lecture'
// });


/**
 *  session config
 */
app.use(express.cookieParser());
app.use(express.session({secret: 'monkey'}));



/**
 *  config
 */
app.http().io();
app.use(express.logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

// app.use(connect.json());
// app.use(connect.urlencoded());
app.use(express.json());
app.use(express.urlencoded());
// app.use(express.bodyParser());



//test page(webRTC)
app.get('/test', function(req, res) {
	res.sendfile(__dirname + '/views/test.html');
});

//test page2(ppt upload)
app.get('/test2', function(req, res) {
	res.sendfile(__dirname + '/views/test_1.html');
});

//test page3(study)
app.get('/study', function(req, res) {
	res.sendfile(__dirname + '/views/study.html');
});


/**
 *  send canvas to others
 */
app.io.route('test', function(req){
	app.io.sockets.emit('test', req.data);
});

/**
 *  send media to others
 */
app.io.route('liveMedia', function(req){
	app.io.sockets.emit('liveMedia', req.data);
});



/**
 *  forward to index.html
 */
app.get('/', function(req, res) {
	res.sendfile(__dirname + '/views/index.html');
});


/**
 *  forward to meeting.html
 */
app.get('/meeting', function(req, res) {
	res.sendfile(__dirname + '/views/meeting.html');
});



/**
 *  send img file to others
 */
app.io.route('upload', function(req){
	req.io.broadcast('upload', req.data);
});

/**
 *  send img file to others
 */
// app.io.route('note', function(req){
// 	console.log(req.data);
// 	connection.connect(function(){
// 		console.log("=========  sucess connection ======");
// 		// get img  path
// 		var query = "select notepath from note where userNo = "+req.data.userNo;
		
// 		// mysql select note query
// 		connection.query(query, function(err, results, fields){
// 			if(err){
// 				msg = 'fail';
// 				req.io.emit('failMsg', msg);
// 				throw err;
// 			}else{
// 				//connection.end();
// 				console.log(results);
// 				req.io.emit('note', results);
// 				req.io.emit('successMsg', msg);
// 				console.log("mysql end ==========");
// 			}
// 		}); //end query
// 	});//end connect
// });


/**
 *  img capture and save path in database 
 */
app.io.route('capture', function(req){

	var base64Data = req.data.img.replace(/^data:image\/png;base64,/,"");
	
	//save id, folder
	var id = req.data.id;
	var roomNum = req.data.roomNum;
	var userNo = req.data.userNo;
	var date = new Date();

	
	//img name
	var imgName = "test.png";
	
	//img save local path
	var filePath = __dirname + "/public/tmp/" +id + "_" + date.getTime() + "_" +imgName; 
	
	require("fs").writeFile(filePath, base64Data, 'base64', function(err) {
		if(err){
			throw err;
		}else{
			// connection.connect(function(){
			// 	console.log("=========  sucess connection ======");
			// 	// img save path
			// 	var path = "http://192.168.2.29:8888/tmp/" +id + "_" + date.getTime() + "_" +imgName;  
			// 	var query = "insert into note (userno, lectureno, notepath) values ('"+ userNo +"','"+ roomNum +"' , '"+ path + "');";
				
			// 	// mysql insert note query
			// 	connection.query(query, function(err, results, fields){
			// 		if(err){
			// 			msg = 'fail';
			// 			req.io.emit('failMsg', msg);
			// 			throw err;
			// 		}else{
			// 			//connection.end();
			// 			req.io.emit('successMsg', msg);
			// 			console.log("mysql end ==========");
			// 		}
			// 	}); //end query
			// });
		} // end if 
	});
});



/**
 * Listen for incoming connections from clients 
 * Start listening for mouse move events 
 * This line sends the event (broadcasts it) to everyone except the originating client.
 */
app.io.sockets.on('connection', function(socket) {

	socket.on('mousemove', function(data) {
		// console.log('ttt', data);
		socket.broadcast.emit('moving', data);
	});

	socket.on('keyboard', function(data) {
		socket.broadcast.emit('keyboard', data);
	});
	
	socket.on('button', function(data) {	
		socket.broadcast.emit('button', data);
	});
	
	socket.on('message', function(data) {
		app.io.sockets.emit('message', data);
	});
});



/**
 *  start server
 */
app.listen(8080, function() {
	console.log("Server Runnig~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!");
});
