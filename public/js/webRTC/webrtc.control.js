$(function() {
	// grab the room from the URL
	//var room = location.search && location.search.split('?')[1];
	// var room = QueryString.title;

	// create our webrtc connection
	var webrtc = new SimpleWebRTC({
		// the id/element dom element that will hold "our" video
		localVideoEl : 'localVideo',
		// the id/element dom element that will hold remote videos
		remoteVideosEl : 'remotes',
		// immediately ask for camera access
		autoRequestMedia : true,
		debug : false,
		detectSpeakingEvents : true,
		autoAdjustMic : false
	});

	// when it's ready, join if we got a room from the URL
	webrtc.on('readyToCall', function() {
		// you can name it anything
		if (room)
			webrtc.joinRoom(room);
	});
});
