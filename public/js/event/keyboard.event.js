/**
 *  if key is 27, input box is hided
 *  if key is 13, write in canvas board
 */
keyboard = function(data) {
	if (data.key == 27) {
		inputs[data.id].val("").hide();
	} else if (data.key == 13) {

		var x = inputs[data.id].offset().left - $('#myCanvas').offset().left;
		var y = inputs[data.id].offset().top - $('#myCanvas').offset().top;
		var maxWidth = inputs[data.id].css('width');
		var lineHeight = data.fontSize + 1;

		maxWidth = maxWidth.substring(0, maxWidth.indexOf("px"));
		wrapText(data.text, x, y + data.fontSize + 3, maxWidth, lineHeight, data.fontSize, data.fontColor);

		inputs[data.id].val("").hide();
	} else {
		inputs[data.id].val(data.text);
	}
};// end keyboard


//�� ������ function
function wrapText(text, x, y, maxWidth, lineHeight, fontSize, fontColor) {
	var line = '';
	ctx.font = fontSize + 'px Times New Roman';
	ctx.strokeStyle = fontColor;
	ctx.fillStyle = fontColor;
	
	for (var n = 0; n < text.length; n++) {
		if (text[n] == '\n') {
			ctx.fillText(line, x, y);
			line = '';
			y += lineHeight;
			continue;
		}
		// ���� �������� ������ �������� ����
		var testLine = line + text[n];
		// 
		var metrics = ctx.measureText(testLine);
		var testWidth = metrics.width;
		if (testWidth > maxWidth && n > 0) {
			ctx.fillText(line, x, y);
			line = text[n];
			y += lineHeight;
		} else {
			line = testLine;
		}
	}
	ctx.fillText(line, x, y);
}