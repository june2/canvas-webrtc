moving = function(data) {
	
	ctx.fillStyle = data.color;
	ctx.strokeStyle = data.color;
	ctx.lineWidth = data.size;
	
	// a new user has come online. create a cursor for them
	if ((!(data.id in clients)) && (id != data.id)) {
		// cursors[data.id] = $('<div class="cursor">').appendTo('#cursors');
		cursors[data.id] = $('<div></div>').appendTo('#cursors');
	}

	// dblclick create a box for them
	// can`t write in others
	if (!(data.id in clients)) {
		if( data.id == id){			
			inputs[data.id] = $('<textarea class="input-text">').appendTo('#inputs');
		} else {
			inputs[data.id] = $('<textarea class="input-text" disabled/>').appendTo('#inputs');
		}
	}
	
	// Move the mouse pointer
	if ((id != data.id)) {
		cursors[data.id].css({
			'left' : data.x,
			'top' : data.y
		});
	}

	// Is the user drawing?
	if (data.drawing && clients[data.id]) {

		// Draw a line on the canvas. clients[data.id] holds
		// the previous position of this user's mouse pointer

	    // draw line
		ctx.beginPath();
		ctx.moveTo(clients[data.id].x, clients[data.id].y);
		ctx.lineTo(data.x, data.y);
		ctx.stroke();
		
		// draw circle
		ctx.beginPath();
		ctx.moveTo(data.x, data.y);
		ctx.arc(data.x, data.y, data.size/2, 0, Math.PI * 2, true);
		ctx.fill();
		ctx.closePath();
		
	}

	//show inputbox event
	if (data.dblclick && clients[data.id]) {

		inputs[data.id].css({
			left : data.x + "px",
			top : data.y + "px"
		}).show();
		inputs[data.id].focus();
		dblclick = false;
	}

	// Saving the current client state
	clients[data.id] = data;
	clients[data.id].updated = $.now();

};// end moving
