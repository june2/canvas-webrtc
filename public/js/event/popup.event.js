/**
 * pop-up event
 */
$(document).ready(function() {

	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').on('click', function() {
		$('#mask , .login-popup').fadeOut(300, function() {
			$('#mask').remove();
		});
		return false;
	});
});

// feedback msg event
successMsg = function() {
	// alert(data);

	
	$('#success-box').fadeIn(300);
	// Getting the variable's value from a link

	// Fade in the Popup and add close button

	// Set the center alignment padding + border
	var popMargTop = ($('#success-box').height() + 24) / 2;
	var popMargLeft = ($('#success-box').width() + 24) / 2;

	$('#success-box').css({
		'margin-top' : -popMargTop,
		'margin-left' : -popMargLeft
	});

	// Add the mask to body
	$('body').append('<div id="mask"></div>');
	$('#mask').fadeIn(3000);
	
	//close popup
	$('#mask , .login-popup').fadeOut(1500, function() {
		$('#mask').remove();
	});
	
	return false;
};

// feedback msg event
failMsg = function() {
	
	$('#fail-box').fadeIn(300);
	// Getting the variable's value from a link
	
	// Fade in the Popup and add close button
	
	// Set the center alignment padding + border
	var popMargTop = ($('#fail-box').height() + 24) / 2;
	var popMargLeft = ($('#fail-box').width() + 24) / 2;
	
	$('#fail-box').css({
		'margin-top' : -popMargTop,
		'margin-left' : -popMargLeft
	});
	
	// Add the mask to body
	$('body').append('<div id="mask"></div>');
	$('#mask').fadeIn(3000);
	
	//close popup
	$('#mask , .login-popup').fadeOut(1500, function() {
		$('#mask').remove();
	});
	
	
	return false;
};
