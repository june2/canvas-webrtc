var QueryString = function() {
	// This function is anonymous, is executed immediately and
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	}
	return query_string;
}();

// The URL of your web server (the port is set in app.js)
var doc = $(document);
var win = $(window);
var canvas = $('#myCanvas');
var ctx = canvas[0].getContext('2d');
ctx.canvas.width  = window.innerWidth;
ctx.canvas.height = window.innerHeight;

var canvas2 = $('#myCanvas2');
var ctx2 = canvas[0].getContext('2d');

// user Info
// var id = QueryString.nickName;
var id = prompt('What is your name?');

// var title = QueryString.title;
var room = prompt('Enter room no.');
var userNo = QueryString.userNo;

// Generate an unique ID
var size = 2;
var color = '#000000';
var fontSize = 10;
var fontColor = '#000000';

// A flag for drawing activity
var drawing = false;
var dblclick = false;

//array 
var clients = {};
var cursors = {};
var inputs = {};

var socket = io.connect();
var prev = {};
var lastEmit = $.now();

var text = "";
var type = "";
var img  = "";

//host address
var host = "http://192.168.0.50:8887/whever_1";

$(function() {
	socket.on('moving', moving);
	socket.on('keyboard', keyboard);
	socket.on('button', button);
	socket.on('upload', upload);
	socket.on('message', message);
	socket.on('test', test);
	socket.on('liveMedia', liveMedia);
	socket.on('note', note);
	socket.on('failMsg', failMsg);
	socket.on('successMsg', successMsg);
	
	// This demo depends on the canvas element
	if (!('getContext' in document.createElement('canvas'))) {
		alert('Sorry, it looks like your browser does not support canvas!');
		return false;
	}
	
	//make video draggable;
    $( "#resizable-local" ).resizable();
    $( "#resizable-local" ).draggable();
    $( "#remotes" ).resizable();
    $( "#remotes" ).draggable();
	
	// chat area
	$('#messageButton').click(function() {
		socket.emit('message', {
			'id' : id,	
			'message' : $('#message').val()
		});
		$('#message').val("");
	});
	
	//listen chatting event
	$('#message').keypress(function(e) {
		if (e.keyCode == 13) {
			socket.emit('message', {
				'id' : id,
				message : $('#message').val(),
			});
			$('#message').val("");
		}
	});
	
	canvas.dblclick('mousedown', function(e) {
		e.preventDefault();
		dblclick = true;
		prev.x = e.pageX - $('#myCanvas').offset().left;
		prev.y = e.pageY - $('#myCanvas').offset().top;
	});
	
	//listen key event
	$('#inputs').keydown(function(e) {
		data = {
			'text' : inputs[data.id].val(),
			'key' : event.keyCode,
			'fontSize' : fontSize,
			'fontColor' : fontColor,
			'id' : id
		};
		socket.emit('keyboard', data);
		keyboard(data);
	});
	
	
	// chat side bar
	$(".side").sidecontent();

	//canvas reset
	$('#clear').on('click', function(e) {
		socket.emit('button', {type : 'clear'});
		button({type : 'clear'});
	});
	
	// use eraser
	$('#eraser').on('click', function(e) {
		color = '#eee';
		size     = 50;	
//		$('#eraser').css('background', 'blue');
//		$('#pen').css('background', 'white');
	});
	
	// use pen
	$('#pen').on('click', function(event) {
		color = '#000000';
		size = 2;
	});
	
	// select pen size
	$('#penSize').on('click', 'li', function(event) {
       size = parseInt(event.currentTarget.attributes.value.value);
       $('#penSizeButton').html('PEN-SIZE : ' + size);
    });
	
	//change pen color
	$('#penColor').on('click', 'li', function(event) {
        color = event.currentTarget.attributes.value.value;
        $('#penColorButton').text('PEN-COLOR : ' + event.currentTarget.children[0].text);
        $('#penColorButton').css('background', color);
        $('#penSizeButton').css('background', color);
	});
	
	//change font size
	$('#fontSize').on('click', 'li', function(event) {
       fontSize = parseInt(event.currentTarget.attributes.value.value);
       $('#input').css('font-size', fontSize + 'px');
       $('#fontSizeButton').html('FONT SIZE : ' + fontSize);
    });
	
	// change font color
	$('#fontColor').on('click', 'li', function(event) {
        fontColor = event.currentTarget.attributes.value.value;
        $('#fontColorButton').text('FONT-COLOR : ' + event.currentTarget.children[0].text);
        $('#fontColorButton').css('background', fontColor);
        $('#fontSizeButton').css('background', fontColor);
    });
	
	// make file path
	$('input[type=file]').change(function () {
		img = '/tmp/' + this.files[0].name; 
	});

	//upload file on canvas
	$('#imagefile').bind('change', function(e){
		
		var filename = $(this).val();
		var lastIndex = filename.lastIndexOf(".");
		
		//get file type
		if (lastIndex >= 0) {
			filename = filename.substring(lastIndex + 1);
			
			//ppt upload logic
			if(filename == "ppt"){
				
				$('#pptUpload').ajaxSubmit({
					url : host+"/pptUpload",
					type : "post",
					dataType : "json",
					contentType : "application/json",
					success: function(data){
						setTimeout(function() { showPpt(data); }, 1000);
						successMsg();
					},
					error: function(){
						failMsg();
					}
				});
			
			//img upload logic
			}else{
				var data = e.originalEvent.target.files[0];
				var reader = new FileReader();
				
				reader.onload = function(evt){
					console.log("====upload");
					upload({img : evt.target.result});
					socket.emit('upload', {img : evt.target.result});
				};
				reader.readAsDataURL(data);
			}//end if
		}//end if
	}); 

	
	//capture img
	$('#capture').on('click', function(e){
		
		var img = canvas.toDataURL("image/png"); 
		console.log("------capture");
		console.log(img+" : " +id);
		
		
		socket.emit('capture', {img : img, 
			 				     id : id,
			 				roomNum : title,
			 				 userNo : userNo});
	});
	
	//load my note
	$('#note').on('click', function(e){
		socket.emit('note',{userNo : userNo});
	});
	
	
	// Remove inactive clients after 1 seconds of inactivity
	setInterval(function() {

		for (ident in clients) {
			if ($.now() - clients[ident].updated > 1000) {

				// Last update was more than 10 seconds ago.
				// This user has probably closed the page
				//cursors[ident].remove();
				delete clients[ident];
				delete cursors[ident];
			}
		}
	}, 1000);
});

// mouseup event
function drawstart(e) {
	e.preventDefault();
	drawing = true;

	prev.x = e.pageX - $('#myCanvas').offset().left;
	prev.y = e.pageY - $('#myCanvas').offset().top;
}

//mousemove event
function drawmove(e) {

	e.preventDefault();
	data = {
		'x' : e.pageX - $('#myCanvas').offset().left,
		'y' : e.pageY - $('#myCanvas').offset().top, 
		'drawing' : drawing,
		'dblclick' : dblclick,
		'color' : color,
		'size' : size,
		'id' : id
	};
	socket.emit('mousemove', data);
	moving(data);
}

//mousedown event
function drawend(e) {
	drawing = false;
}

//init canvas
function init() {
	canvas = document.getElementById('myCanvas');
	
	canvas.addEventListener('mousedown', drawstart);
	canvas.addEventListener('mousemove', drawmove);
	canvas.addEventListener('mouseup', drawend, false);

	canvas.addEventListener('touchstart', drawstart);
	canvas.addEventListener('touchmove', drawmove);
	canvas.addEventListener('touchend', drawend, false);
}


//ppt append evnet
function showPpt(data){
	
    text = '<div class="PPT-row">';
	$.each(data, function(index, element) {
		console.log(element.pptPath);
		console.log(index);
		text += '<img id = "PPT-img'+index+'" src="'  +host+ element.pptPath + '" onclick="PPTbroadcast(this.src);"><br/>';
	});

	text += '</div>';
	$('#img').append(text);
}

//ppt broadcast to others
function PPTbroadcast(src){
	console.log("dddd");
	console.log(src);
	upload({img : src});
	socket.emit('upload', {img : src});
}


